# frozen_string_literal: true

# This file is part of Kéfir's URL shortener.
#
# Kéfir's URL shortener is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Foobar is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General Public License
# along with Kéfir's URL shortener.  If not, see
# <http://www.gnu.org/licenses/>.

require 'dotenv'
require 'sinatra'
require 'securerandom'
require_relative 'lib/url_shortener'

Dotenv.load

$url_shortener = URLShortener.new

# El index del sitio, ver views/index.haml
get '/' do
  haml :index
end

get '/cuidados' do
  haml :cuidados
end

get '/care' do
  haml :care
end

get '/indexEN' do
  haml :indexEN
end

# Al pedir una URL acortada, buscarla en la base de datos y responder
# con una redirección
get '/:token' do
  @url = $url_shortener.url_from_token(params[:token])

  # Si la URL no existe, ir al índice
  redirect to('/') if @url.nil?
  # Si es phishing, enviar a otro lado
  @phish = $url_shortener.dont_be_phished(@url)
  if @phish
    haml :oops
  else
    redirect @url, 307
  end
end

# Devuelve una versión en texto plano
post '/txt' do
  auth  = params['auth']
  token = $url_shortener.shorten(params[:url])
  return unless auth == ENV.fetch('AUTH_TOKEN', SecureRandom.hex)

  url("/#{token}") + "\n"
end

post '/delete' do
  token = params['token']
  auth  = params['auth']

  # comparar la autenticación enviada con la configurada o devolver
  # algo random
  return unless auth == ENV.fetch('AUTH_TOKEN', SecureRandom.hex)

  'OK' if $url_shortener.delete(token)
end
