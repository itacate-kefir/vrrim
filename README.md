
# Instalar
  git clone https://0xacab.org/kefir-libre/url_shortener
  cd url_shortener
  bundle install --path=~/.gem

si no está bundler, instalarlo:
  sudo sh -c 'umask 022 ; gem install --no-user-install bundler'

# Correr

  bundle exec rackup

navegar a http://localhost:9292


# Deploy al server

./bin/cap production deploy



