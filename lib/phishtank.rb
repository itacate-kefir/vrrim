# frozen_string_literal: true

# This file is part of Kéfir's URL shortener.
#
# Kéfir's URL shortener is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Foobar is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General Public License
# along with Kéfir's URL shortener.  If not, see
# <http://www.gnu.org/licenses/>.

require 'net/https'
require 'uri'
require 'json'

class PhishTank
  CHECK_URL = 'https://checkurl.phishtank.com/checkurl/'.freeze

  def initialize(url)
    @api_key = ENV['PHISHTANK_API_KEY']
    @url = encode_url(url)
  end

	def lookup!
		check_url = encode_url(CHECK_URL)

		res = Net::HTTP.post_form(check_url, post_params)

		@results = JSON.parse(res.body)['results']
	end

  def results
    @results
  end

  def details
    @results['phish_detail_page']
  end

  def phishing?
    @results['in_database']
  end

	private

	def post_params
		{
			url: @url,
			app_key: @api_key,
			format: 'json'
		}
	end

	def encode_url(url)
		URI(URI.encode(url))
	end
end
