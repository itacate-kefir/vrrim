# frozen_string_literal: true

# This file is part of Kéfir's URL shortener.
#
# Kéfir's URL shortener is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Foobar is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General Public License
# along with Kéfir's URL shortener.  If not, see
# <http://www.gnu.org/licenses/>.

require 'dbm'
require 'uri'
require 'securerandom'
require_relative 'phishtank'

# Acortar URLs
class URLShortener
  def initialize(database = 'urls')
    @db ||= DBM.open(database, 0640, DBM::WRCREAT)

    self
  end

  def shorten(url)
    url = prefix_protocol(url)
    return unless valid? url
    # Si la URL ya está en la base de datos, devolver la misma versión
    # corta
    @db.key(url) || store_token(find_token, url)
  end

  # Obtener la URL guardada a partir de un token
  def url_from_token(token)
	  url = @db[token]
  end

  def dont_be_phished(url)
    check_url = PhishTank.new(url)
    check_url.lookup!

    if check_url.phishing?
      check_url.details
    end
  end

  def validate(url)
    url if valid? url
  end

  def prefix_protocol(url)
    /\Ahttps?:\/\// =~ url ? url : "http://#{url}"
  end

  def delete(token)
    @db.delete(token)
  end

  private

  def store_token (token, url)
    @db[token] = url

    token
  end

  # Validar la URL
  def valid?(url)
    !URI.parse(URI.encode(prefix_protocol(url))).host.nil?
  end

  # Encontrar la cantidad de caracteres necesarios para generar un
  # token disponible
  def find_base
    base = 1
    loop do
      # Como usamos hexadecimales, elevamos la cantidad de caracteres
      # disponibles a la base y chequeamos si la cantidad de URLs es
      # menor, si no aumentamos y repetimos.
      #
      # A la base se le suma 1 porque hex(1) devuelve dos caracteres.
      break if @db.length < (16**(base + 1))
      base += 1
    end

    base
  end

  def find_token
    token = nil
    # Si no está en la base de datos, encontrar un token disponible
    loop do
      # Crear un token aleatorio usando la base que encontramos
      token = SecureRandom.hex(find_base)
      # Crear otro token si ya existe
      next if @db.has_key?(token)
      break
    end

    token
  end
end
