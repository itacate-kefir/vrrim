# frozen_string_literal: true
set :application, 'vrrim'
set :repo_url, 'https://0xacab.org/itacate-kefir/vrrim'

set :bundle_flags, '--deployment'
set :default_env, path: '/usr/lib/passenger/bin:$PATH'

set :linked_files, %w{urls.dir urls.pag .env}
